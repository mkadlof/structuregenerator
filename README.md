# README #

This is simple generator of different types of initial structurs in PDB file format for various molecular dynamics experiments.
It also contain tool for ploting distance map from generated structure.

### Dependencies and setup ###

python 3.6.x

	pip install -r requirements.txt
	python setup.py build_ext -i

### Contact to main author ###

Michał Kadlof <m.kadlof@cent.uw.edu.pl>

### Significant contributors ###

Grzegorz Bokota <g.bokota@cent.uw.edu.pl>