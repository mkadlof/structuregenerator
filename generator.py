#! /usr/bin/env python3

import argparse
import datetime
import os
import sys
import time
from itertools import combinations

import numpy as np
from numpy import pi, sin, cos, sqrt
from scipy.interpolate import interpn, RegularGridInterpolator
from scipy.linalg import orth
from scipy.spatial.distance import pdist, squareform
from tqdm import tqdm

from generator_fast.generator_fast import correct_distances as correct_distances2
from points_io import save_points_as_pdb

__author__ = "Michał Kadlof <m.kadlof@cent.uw.edu.pl"


def line(n):
    points = []
    for i in range(n):
        points.append([i, 0, 0])
    return np.array(points)


def random_versor():
    x = np.random.uniform(-1, 1)
    y = np.random.uniform(-1, 1)
    z = np.random.uniform(-1, 1)
    d = (x ** 2 + y ** 2 + z ** 2) ** 0.5
    return np.array([x / d, y / d, z / d])


def dist(p1, p2):
    x1, y1, z1 = p1
    x2, y2, z2 = p2
    return ((x1 - x2) ** 2 + (y1 - y2) ** 2 + (z1 - z2) ** 2) ** 0.5  # faster than np.linalg.norm


def polymer_circle(n, z_stretch=0):
    points = []
    angle_increment = 360 / float(n)
    radius = 1 / (2 * sin(np.radians(angle_increment) / 2.))  # assure distance 1
    z_stretch = z_stretch/n
    z = 0
    for i in range(n):
        x = radius * cos(angle_increment * i * pi / 180)
        y = radius * sin(angle_increment * i * pi / 180)
        if z_stretch != 0:
            z += z_stretch
        points.append((x, y, z))
    return points


def self_avoiding_random_walk(n, step=1, bead_radius=0.5, epsilon=0.001):
    points = [np.array([0, 0, 0])]
    for i in tqdm(range(n - 1)):
        step_is_ok = False
        while not step_is_ok:
            potential_new_step = points[-1] + step * random_versor()
            for j in points:
                d = dist(j, potential_new_step)
                if d < 2 * bead_radius - epsilon:
                    break
            else:
                step_is_ok = True
        points.append(potential_new_step)
    return points


def spiral_sphere(n, r, c):
    """based on: http://elib.mi.sanu.ac.rs/files/journals/vm/57/vmn57p2-10.pdf"""

    def calc_x(t): return sqrt(r ** 2 - t ** 2) * cos(t / c)

    def calc_y(t): return sqrt(r ** 2 - t ** 2) * sin(t / c)

    def calc_z(t): return t

    t_lin = np.linspace(-r, r, n)
    x = calc_x(t_lin).reshape((n, 1))
    y = calc_y(t_lin).reshape((n, 1))
    z = calc_z(t_lin).reshape((n, 1))
    points = np.concatenate((x, y, z), 1)
    return points


def baseball(n, r):
    """based on: http://paulbourke.net/geometry/baseball/"""
    b = pi / 2
    a = 0.4  # shape parameter. Better not to change

    def calc_x(t): return r * sin(b - (b - a) * cos(t)) * cos(t / 2. + a * sin(2 * t))

    def calc_y(t): return r * sin(b - (b - a) * cos(t)) * sin(t / 2. + a * sin(2 * t))

    def calc_z(t): return r * cos(b - (b - a) * cos(t))

    t_lin = np.linspace(0, 4 * pi - (4 * pi) / n, n)
    x = calc_x(t_lin).reshape((n, 1))
    y = calc_y(t_lin).reshape((n, 1))
    z = calc_z(t_lin).reshape((n, 1))
    points = np.concatenate((x, y, z), 1)
    return points


def random_gas(n, params):
    if len(params) == 1:
        d = params[0]
        x = np.random.uniform(-d, d, n).reshape((n, 1))
        y = np.random.uniform(-d, d, n).reshape((n, 1))
        z = np.random.uniform(-d, d, n).reshape((n, 1))
    elif len(params) == 6:
        x1, y1, z1, x2, y2, z2 = params
        x = np.random.uniform(x1, x2, n).reshape((n, 1))
        y = np.random.uniform(y1, y2, n).reshape((n, 1))
        z = np.random.uniform(z1, z2, n).reshape((n, 1))
    else:
        raise ValueError('Incorrect input!')
    points = np.concatenate((x, y, z), axis=1)
    return points


def random_walk(n):
    versors = np.random.uniform(-1, 1, 3 * (n - 1)).reshape(((n - 1), 3))
    points = [[0, 0, 0]]
    for i in versors:
        points.append([points[-1][0] + i[0], points[-1][1] + i[1], points[-1][2] + i[2]])
    return np.array(points)


def constant_angle(n, theta, step=1):
    def orthogonal_planes(ab, B):
        ab1 = ~(np.absolute(ab) < 10e-5)
        if ab1[0]:
            X = np.array([ab[1], -ab[0], 0])
            Y = np.array([ab[2], 0, -ab[0]])
            ort = orth(np.array([X, Y]).T)
            X, Y = ort.T * B
            return X, Y
        elif ab1[1]:
            X = np.array([1, 0, 0]) * B
            Y = np.array([0, ab[2], -ab[1]])
            Y = Y / np.linalg.norm(Y) * B
            return X, Y
        else:
            return np.array([1, 0, 0]) * B, np.array([0, 1, 0]) * B

    theta = np.deg2rad(theta)
    points = [np.array([0, 0, 0]), np.array([step, 0, 0])]
    r = np.linalg.norm(points[1] - points[0])
    A = r * np.cos(np.pi - theta)
    B = r * np.sin(np.pi - theta)
    for i in range(n - 2):
        a = points[-2]
        b = points[-1]
        ab = b - a
        X, Y = orthogonal_planes(ab, B)
        angle = 2 * np.pi * np.random.random()
        new_point = X * np.sin(angle) + Y * np.cos(angle) + (b + ab / np.linalg.norm(ab) * A)
        points.append(new_point)
    return np.array(points)


def img_gas(n, filename, voxel_size, threshold):
    if not os.path.exists(filename):
        sys.exit(1, f'File {filename} does not exists.')
    img = np.load(filename)
    coords = [np.arange(img.shape[0]), np.arange(img.shape[1]), np.arange(img.shape[2])]
    points = []
    rand_max = np.array(img.shape) - 1
    while len(points) < n:
        new_points = np.random.uniform((0, 0, 0), rand_max, size=(20, 3))
        good_pooints = interpn(coords, img, new_points) > threshold
        points.extend(new_points[good_pooints][:, ::-1])
    points = points[:n]
    points = np.array(points)
    return points * voxel_size


def correct_distances(points, distances, img, voxel_size, threshold=1, precision=3):
    """If segment between beads is outside of image, set a large distance in distance matrix.
        precision - number of points to test between two points
    """
    coords = [np.arange(img.shape[0]) * voxel_size[2], np.arange(img.shape[1]) * voxel_size[1],
              np.arange(img.shape[2]) * voxel_size[0]]
    grid = RegularGridInterpolator(coords, img)

    for i, j in tqdm(list(combinations(range(len(points)), 2))):
        testing_x = np.linspace(points[i][0], points[j][0], precision + 2)[1:-1]
        testing_y = np.linspace(points[i][1], points[j][1], precision + 2)[1:-1]
        testing_z = np.linspace(points[i][2], points[j][2], precision + 2)[1:-1]
        if np.any(grid(list(zip(testing_z, testing_y, testing_x))) < threshold):
            distances[i, j] *= 1e3
            distances[j, i] *= 1e3
    return distances


def img_ts(n, filename, voxel_size, threshold):
    from tsp_solver.greedy import solve_tsp
    print('WARNING! This img_ts is still under development. Precision hardcoded!')
    print('Randomization...')
    points = img_gas(n, filename, voxel_size, threshold)
    print('Calculating distances...')
    distances = squareform(pdist(points))
    distances2 = np.copy(distances)
    img = np.load(filename)
    # distances = correct_distances_old(points, distances, img, voxel_size, threshold, precision=3)
    distances2 = correct_distances2(points, distances2, img, voxel_size, threshold / 2, precision=100)
    print('TSP solver...')
    path = solve_tsp(distances2)
    new_points = [0] * len(path)
    for i in range(len(path)):
        new_points[i] = points[path[i]]
    return new_points


def center(points, centering_point):
    centering_point = np.array(centering_point)
    center_before_centering = np.mean(points, axis=0)
    centering_vector = centering_point - center_before_centering
    points = points + centering_vector
    return points


def build(args):
    if args.N > 9999:
        sys.exit("9999 beads is the limit! You requested for {}".format(args.N))
    error_msg = "Wrong number of parameters for {{}}. Please consult {0} -h".format(sys.argv[0])
    render_connect = True  # default value for save_points_as_pdb()

    if args.type == "line":
        if len(args.params) != 0:
            sys.exit(error_msg.format(args.type))
        if args.step != 1:
            raise NotImplementedError("Step size is not implemented yet for {}".format(args.type))
        points = line(args.N)

    elif args.type == "circle":
        if len(args.params) != 1:
            sys.exit(error_msg.format(args.type))
        if args.step != 1:
            raise NotImplementedError("Step size is not implemented yet for {}".format(args.type))
        z = float(args.params[0])
        points = polymer_circle(args.N, z)

    elif args.type == "spiral_sphere":
        if len(args.params) != 2:
            sys.exit(error_msg.format(args.type))
        if args.step != 1:
            raise NotImplementedError("Step size is not implemented yet for {}".format(args.type))
        r = float(args.params[0])
        c = float(args.params[1])
        points = spiral_sphere(args.N, r, c)

    elif args.type == "baseball":
        if len(args.params) != 1:
            sys.exit(error_msg.format(args.type))
        if args.step != 1:
            raise NotImplementedError("Step size is not implemented yet for {}".format(args.type))
        r = float(args.params[0])
        points = baseball(args.N, r)

    elif args.type == "gas":
        if len(args.params) not in [1, 6]:
            sys.exit(error_msg.format(args.type))
        if args.step != 1:
            raise NotImplementedError("Step size is not implemented yet for {}".format(args.type))
        points = random_gas(args.N, args.params)
        render_connect = False

    elif args.type == "random_walk":
        if len(args.params) != 0:
            sys.exit(error_msg.format(args.type))
        if args.step != 1:
            raise NotImplementedError("Step size is not implemented yet for {}".format(args.type))
        points = random_walk(args.N)

    elif args.type == "self_avoiding_random_walk":
        if len(args.params) != 0:
            sys.exit(error_msg.format(args.type))
        points = self_avoiding_random_walk(args.N, step=args.step, bead_radius=args.step / 2)

    elif args.type == "constant_angle":
        if len(args.params) != 1:
            sys.exit(error_msg.format(args.type))
        fi = float(args.params[0])
        points = constant_angle(args.N, fi, step=args.step)

    elif args.type == "img_gas":
        if len(args.params) != 5:
            sys.exit(error_msg.format(args.type))
        file_name = args.params[0]
        voxel_size = np.array(args.params[1:4], dtype=np.float)
        threshold = float(args.params[4])
        points = img_gas(args.N, file_name, voxel_size, threshold)
        render_connect = False

    elif args.type == "img_ts":
        if len(args.params) != 5:
            sys.exit(error_msg.format(args.type))
        file_name = args.params[0]
        voxel_size = np.array(args.params[1:4], dtype=np.float)
        threshold = float(args.params[4])
        points = img_ts(args.N, file_name, voxel_size, threshold)

    else:
        print("Wrong type {}. Please consult {} -h".format(args.type, sys.argv[0]))
        sys.exit(1)

    if args.center:
        points = center(points, args.center)
    elif not args.center and args.center_pdb:
        points = center(points, [4500, 4500, 4500])

    timestamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
    remarks = [f'CREATED WITH STRUCTURE GENERATOR at {timestamp}']
    save_points_as_pdb(points, args.output, save_psf=args.psf, render_connect=render_connect, remarks=remarks)


def main():
    longer_help = """
    Available types and its additional params:
    line:
        no extra args
    circle:
        z - z_streatch
    spiral_sphere:
        r - radius of sphere (float)
        c - controls number of turns (float)
    baseball:
        r - radius of sphere (float)
    gas:
        d - dimension of box (-d, d)
          or
        x1, y1, z1, x2, y2, z2 - box coordinates (floats)
    random_walk:
        no extra args
    self_avoiding_random_walk:
        no extra args
    constant_angle:
        theta - angle (degrees)
    img_gas:
        filename - npy file
        vx, vy, vz - voxel size
        threshold
    img_ts (points connected with traveling salesman algorithm):
        filename - npy file
        vx, vy, vz - voxel size
        threshold
    """

    parser = argparse.ArgumentParser(description="Generate initial structure for polymer MD simulations",
                                     epilog=longer_help, formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument("-c", '--center', nargs=3, metavar=('X', 'Y', 'Z'), type=float,
                        help="Center in x, y, z. or in [0, 0, 0] if no value provided")
    parser.add_argument("-C", '--center_pdb', action='store_true', help="Center in in PDB box [4500, 4500, 4500].")
    parser.add_argument("-o", '--output', default="initial_structure.pdb", help="output PDB file name")
    parser.add_argument("-p", '--psf', action="store_true", help="generate PSF file")
    parser.add_argument("-s", '--step', type=float, default=1.0, help="step size (default: 1)")
    parser.add_argument("type", help="One of the listed below")
    parser.add_argument("N", type=int, help="number of beads")
    parser.add_argument("params", metavar='params', nargs='*',
                        help="Additional params type dependent params")
    args = parser.parse_args()
    build(args)


if __name__ == '__main__':
    main()
