# cython: boundscheck=False, wraparound=False, nonecheck=False, cdivision=True
# distutils: define_macros=CYTHON_TRACE_NOGIL=1

import numpy as np
cimport numpy as np

from libc.math cimport floor
from libc.stdlib cimport malloc, free

ctypedef fused numpy_types:
    np.float32_t
    np.float64_t

ctypedef fused img_types:
    np.uint8_t
    np.uint16_t
    np.uint32_t
    np.int8_t
    np.int16_t
    np.int32_t
    np.float32_t
    np.float64_t


cdef float interpolate(img_types * values, float z, float y, float x):
    cdef float tmp1, tmp2, tmp3, tmp4
    tmp1 = values[0] * (1 - y) + values[1] * y
    tmp2 = values[3] * (1 - y) + values[2] * y
    tmp3 = values[4] * (1 - y) + values[5] * y
    tmp4 = values[7] * (1 - y) + values[6] * y
    tmp1 = tmp1 * (1-x) + tmp2 * x
    tmp3 = tmp3 * (1-x) + tmp4 * x
    return tmp1 * (1-z) + tmp3 * z


def correct_distances(np.ndarray[numpy_types, ndim=2] points, np.ndarray[np.float64_t, ndim=2] distances, np.ndarray[img_types, ndim=3] img, voxel_size, float threshold=1, int precision=3):
    """If segment between beads is outside of image, set a large distance in distance matrix.
        precision - number of points to test between two points
    """
    cdef Py_ssize_t i, j, arr_len, xf, yf, zf
    cdef float frac, x,y,z, val, pr
    min_val = voxel_size.min()
    voxel_size2 = voxel_size / min_val
    cdef np.ndarray[numpy_types, ndim=2] points2 = points/voxel_size
    arr_len = points.shape[0]
    cdef img_types * values = <img_types *> malloc(sizeof(img_types) * 8)

    for i in range(arr_len):
        for j in range(i+1, arr_len):
            for pr in range(1, precision+1):
                frac = (pr / (precision + 1))
                x = (points2[i, 0] * frac  + (points2[j, 0] * (1 - frac )))
                y = (points2[i, 1] * frac  + (points2[j, 1] * (1 - frac )))
                z = (points2[i, 2] * frac  + (points2[j, 2] * (1 - frac )))
                xf = <Py_ssize_t> floor(x)
                yf = <Py_ssize_t> floor(y)
                zf = <Py_ssize_t> floor(z)
                values[0] = img[zf, yf, xf]
                values[1] = img[zf, yf+1, xf]
                values[2] = img[zf, yf+1, xf+1]
                values[3] = img[zf, yf, xf+1]
                values[4] = img[zf+1, yf, xf]
                values[5] = img[zf+1, yf+1, xf]
                values[6] = img[zf+1, yf+1, xf+1]
                values[7] = img[zf+1, yf, xf+1]
                val = interpolate(values, z-zf, y-yf, x-xf)
                if val < threshold:
                    distances[i, j] = 1e12
                    distances[j, i] = 1e12
    free(values)
    return distances

def interpolate_test(np.ndarray[img_types, ndim=3] test_val, float z, float y, float x):
    cdef img_types * values = <img_types *> malloc(sizeof(img_types) * 8)
    values[0] = test_val[0, 0, 0]
    values[1] = test_val[0, 1, 0]
    values[2] = test_val[0, 1, 1]
    values[3] = test_val[0, 0, 1]
    values[4] = test_val[1, 0, 0]
    values[5] = test_val[1, 1, 0]
    values[6] = test_val[1, 1, 1]
    values[7] = test_val[1, 0, 1]
    res = interpolate(values, z, y, x)
    free(values)
    return res