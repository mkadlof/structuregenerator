from distutils.core import setup
from Cython.Build import cythonize
from distutils.extension import Extension
import numpy as np

extensions = [
    Extension("generator_fast.generator_fast", ["generator_fast/generator_fast.pyx"],
        include_dirs = [np.get_include()])
    ]

setup(
    ext_modules = cythonize(extensions),
    name="generator fast"
)